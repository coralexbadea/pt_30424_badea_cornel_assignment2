package com.project2.scheduler;

/**@enum SelectionPolicy - Enumeration that helps with the selection of the
 * desired com.com.project2.strategy*/
public enum SelectionPolicy {
    SHORTEST_QUEUE, SHORTEST_TIME;
}
