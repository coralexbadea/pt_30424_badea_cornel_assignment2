package com.project2.scheduler;

import com.project2.server.Client;
import com.project2.server.QueueServer;
import com.project2.strategy.ConcreteStrategyQueue;
import com.project2.strategy.ConcreteStrategyTime;
import com.project2.strategy.Strategy;

import java.util.ArrayList;
import java.util.List;

/**@class Scheduler - It's main purpose is to send clients(tasks) to the a Server
 * chosen according to the established com.com.project2.strategy, also to start and stop the
 * queueServer threads*/
public class Scheduler {
    //the list of all servers
    private List<QueueServer> queueServers;
    //the total number of servers
    private int maxNoServers;
    //the used com.com.project2.strategy
    private Strategy strategy;



    public Scheduler(int maxNoServers, int maxTasksPerServers, Strategy strg){
        this.maxNoServers = maxNoServers;
        this.queueServers = new ArrayList<>();

        for(int i = 0 ; i < maxNoServers; i++){
            QueueServer queueServer = new QueueServer(maxTasksPerServers);
            this.queueServers.add(queueServer);
            //create thread with the object
            Thread t = new Thread(queueServer);
            //start the thread
            t.start();
        }
        //choose the current com.com.project2.strategy
        this.strategy = strg;
    }

    /*finishThreads method - Used for finishing the existing threads*/
    public void finishThreads(){
       for(QueueServer queueServer : queueServers)
           queueServer.stop();
//        for(QueueServer queueServer : queueServers)
//            synchronized (queueServer.getClients()){
//                queueServer.getClients().notify();
//            }

    }
    //changeStrategy method- auxiliary method that can be used for changing the current use com.com.project2.strategy
    /*
    public void changeStrategy(SelectionPolicy policy){
        if(policy == SelectionPolicy.SHORTEST_QUEUE){
            strategy = new ConcreteStrategyQueue();
        }
        else if(policy == SelectionPolicy.SHORTEST_TIME){
            strategy = new ConcreteStrategyTime();
        }
    }
    */
    /*dispatchClient - This method is used for sending a client to an appropiate com.com.project2.server
    depending on the chosen com.com.project2.strategy
     */
    public void dispatchClient(Client c){
        this.strategy.addTask(queueServers, c);
    }

    //getter for the list of queue servers
    public List<QueueServer> getQueueServers(){
        return queueServers;

    }

}
