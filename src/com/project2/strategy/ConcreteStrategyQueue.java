package com.project2.strategy;

import com.project2.server.Client;
import com.project2.server.QueueServer;

import java.util.List;

/**
 * @class ConcreteStrategyQueue - Implements Strategy interface. It contains the logic
 *  used for queueing the clients when they arrive depending on the size of the queue */
public class ConcreteStrategyQueue implements Strategy{

    public void addTask(List<QueueServer> queueServers, Client client){
        QueueServer minSizeQueue = queueServers.get(0);
        //get the min size queue
        for(QueueServer queue : queueServers){
            if(minSizeQueue.getSize() > queue.getSize())
                minSizeQueue = queue;
        }
        //add a task to it

        minSizeQueue.addClient(client);
    }
}
