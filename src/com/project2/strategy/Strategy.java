package com.project2.strategy;

import com.project2.server.Client;
import com.project2.server.QueueServer;

import java.util.List;

public interface Strategy {
    public void addTask(List<QueueServer> queueServer, Client client);
}
