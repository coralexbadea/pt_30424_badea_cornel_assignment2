package com.project2.strategy;

import com.project2.server.Client;
import com.project2.server.QueueServer;

import java.util.List;

/**
 * @class ConcreteStrategyQueue - Implements Strategy interface. It contains the logic
 *  used for queueing the clients when they arrive depending on the average waiting
 *  time of the queue at that time */
public class ConcreteStrategyTime implements Strategy {
    public void addTask(List<QueueServer> queueServers, Client client){
        QueueServer minTimeQueue = queueServers.get(0);
        //get the min time queue
        for(QueueServer queue : queueServers){
            if(minTimeQueue.getTime() > queue.getTime())
                minTimeQueue = queue;
        }
        //add a task to it

        minTimeQueue.addClient(client);
    }
}
