package com.project2.simulation;

import com.project2.scheduler.Scheduler;
import com.project2.server.Client;
import com.project2.server.QueueServer;
import com.project2.strategy.ConcreteStrategyQueue;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;


/**
 * @class SimulationManager - Main class that does the following:
 * generates randomly the tasks based on the contents of the input file;
 * generates the com.com.project2.simulation internal time that is used for scheduling all the client entrances;
 * calls com.com.project2.scheduler to dispatch tasks;
 * contains the com.com.project2.simulation loop;
 * updates the User Interface;
 * */
public class SimulationManager implements Runnable{
    //maximum time of the com.com.project2.simulation
    private int timeLimit ;
    private int numberOfQueueServers ;
    private int numberOfClients ;
    private String outputFile; //the output file

    //entity responsable wth queue management and client distribution
    private Scheduler scheduler;
    //list of randomly generated clients
    private List<Client> generateClients;

    /**
     * checkArgs method - This method is used about to check the number of arguments
     * given to the program
     * @param args - the array of string arguments*/
    public void checkArgs(String[] args){
        if(args.length != 2){
            System.out.println("Usage:java Fir.java <input file> <output file> ");
            System.exit(1);
        }

    }

    /**
     * writeFile method - This method is ued to write in the output file in a specific manner
     * given a list of strings
     * @param strings - list of strings to be parsed and written to the file
     * @param fileName - the fileName of the file to write to */
    private void writeFile(List<String> strings,String fileName)  {
    try { // it can give IOException
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
        writer.write(strings.get(0));
        writer.newLine();
        writer.write(strings.get(1));
        writer.newLine();

        for(int i = 0; i < numberOfQueueServers ; i++){

            writer.write(strings.get(i + 2));
            writer.newLine();
        }
        writer.newLine();
        writer.close();
    }
    catch (Exception e)
    {
        System.err.format("Exception occurred trying to write'%s'.", fileName);
        e.printStackTrace();
        System.exit(4);

    }


    }

    /**
     * readFile method -  This method is used to read from a given input file
     * @param fileName - this parameter is the file name of the input file as a String
     * @return  -  a list with all the stings read from the input file*/
    private List<String> readFile(String fileName)
    {

        List<String> records = new ArrayList<>();
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line;
            while ((line = reader.readLine()) != null)
            {
                records.add(line);
            }
            reader.close();
            if(records.size() != 5){
                System.err.format("Wrong file format in file: '%s'.", fileName);
                System.exit(2);
            }

            return records;
        }
        catch (Exception e)
        {
            System.err.format("Exception occurred trying to read '%s'.", fileName);
            e.printStackTrace();
            System.exit(3);
            return null;
        }
    }



    public SimulationManager(String[] args){
        //check the arguments
        checkArgs(args);
        generateClients = new ArrayList<>();
        //get the input strings;
        List<String> inputStrings = readFile(args[0]);
        this.outputFile = args[1];
        //initialize variables acording to the read strings form the input file
        this.timeLimit = Integer.parseInt(inputStrings.get(2));
        this.numberOfClients = Integer.parseInt(inputStrings.get(0));
        this.numberOfQueueServers = Integer.parseInt(inputStrings.get(1));
        //initialize the com.com.project2.scheduler
        //  => create and start numberOfServers threads
        //  => initialize selection com.com.project2.strategy
        this.scheduler = new Scheduler(numberOfQueueServers, numberOfClients, new ConcreteStrategyQueue());
        //generate numberOfClients clients using generateNRandomTasks()
        //and store them to generatedTasks
        generateNRandomTasks(Integer.parseInt(inputStrings.get(0)), inputStrings.get(3), inputStrings.get(4));


    }
    //utilitary method used for sorting a list of Clients in ascending order
    class Sortbyarrival implements Comparator<Client>
    {

        public int compare(Client a, Client b)
        {
            return a.getArrivalTime() - b.getArrivalTime();
        }
    }

    /**
     * generateNRandomTasks method - This method is generating an arbitrary number of
     * clients with random attributes within the given bounds
     * @param  noClients - the number of clients to be generated
     * @param arivalString  - string that contains the bounds(separated by a comma) of the value that arrivalTime can have
     * @param serviceString  - string that contains the bounds(separated by a comma) of the value that serviceTime can have */
    private void generateNRandomTasks(int noClients, String arivalString, String serviceString){
        int ID = 1; // initial ID
        String[] arivalInterval = arivalString.split(",", 2);
        String[] serviceInterval = serviceString.split(",", 2);

        if(arivalInterval.length != 2 || serviceInterval.length !=2 ) {
            System.err.format("Wrong input file format");
            System.exit(2);
        }
        //generate N random tasks:
        Client aux;
        Random rand = new Random();
        int arivalTime = 0;
        int serviceTime = 0;
        for(int i = 0 ; i < noClients; i++){
            try { //generate arivalTine and serviceTime values
                arivalTime = rand.nextInt(
                        Integer.parseInt(arivalInterval[1]) -
                                Integer.parseInt(arivalInterval[0]) + 1)
                        + Integer.parseInt(arivalInterval[0]);
                serviceTime = rand.nextInt(
                        Integer.parseInt(serviceInterval[1]) -
                                Integer.parseInt(serviceInterval[0]) + 1)
                        + Integer.parseInt(serviceInterval[0]);
            }
            catch (Exception e){
                System.err.format("Wrong input file format");
                e.printStackTrace();
                System.exit(2);
            }
            //generate a new client according to the obtained values
            aux = new Client(ID++, arivalTime, serviceTime);//also increment ID
            generateClients.add(aux); //add to the list
        }

        //sort list with respect to arrivalTime
        Collections.sort(generateClients, new Sortbyarrival());
    }

    /**
     * updateUI method - This method is used processing for printing the necessary
     * strings to the output file
     * @param currentTime - the current time of the com.com.project2.simulation
     * @param outputFile - the output file as a string
     */
    private void updateUI(int currentTime, String outputFile){
        //the strings to be printed in the output file
        List<String> stringsToWrite = new ArrayList<>();

        stringsToWrite.add("Time: " + currentTime); //the time string

        String auxString = "Waiting clients: ";// the waiting clients string
        for(Client client : generateClients){
           auxString += client.getStringClient();
        }
        stringsToWrite.add(auxString);
        int i = 1;
        auxString = "";
        for(QueueServer queueServer : scheduler.getQueueServers()){
            auxString += ("Queue " + i +": "); // ith queue string
            auxString += queueServer.getStringQueue();
            i++;

            stringsToWrite.add(auxString);
            auxString = "";

        }

        writeFile(stringsToWrite, outputFile);

    }
    /**
     * lastUpdateUI method - this method is used for printing to the output file
     * the final average waiting time
     * @param avgWaitingTime - the average waiting time to be printed*/
    private void lastUpdateUI(float avgWaitingTime){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(this.outputFile, true));
            writer.write(Float.toString(avgWaitingTime));
            writer.close();
        }
        catch (Exception e)
        {
            System.err.format("Exception occurred trying to write'%s'.", this.outputFile);
            e.printStackTrace();
            System.exit(4);

        }
    }


    /**
     * getAvgWaiting method - This method is used to calculate the average waiting time
     * by summing the waiting time across the queues and dividing by the number of clients
     * @return  - the calculated average waiting time */
    private float getAvgWaiting() {
        float avgWaitingTime = 0;
        List<QueueServer> queueServers = scheduler.getQueueServers();
        for(QueueServer queue : queueServers){
            avgWaitingTime += queue.getWaitingTime();
        }
        avgWaitingTime = avgWaitingTime / numberOfClients;
        return avgWaitingTime;

    }

    /**
     * (Deprecated) notifyQueuesServers method - method used notifying the closed queueServers
     * that a new client is dispached*/
//    private void notifyQueueServers(){
//
//        for(QueueServer queueServer: scheduler.getQueueServers()){
//            synchronized (queueServer.getClients()){
//                queueServer.getClients().notify();
//            }
//        }
//    }

    //Overwriting the run method of the Runnable interface
    public void run(){
        //initializing the current time of the com.com.project2.simulation
        int currentTime = 0;
        while (currentTime < timeLimit){  //main loop of the com.com.project2.simulation

            // iterate generatedTasks list and pick that have the
            //arrivatTime equal with the currentTime
            Iterator itr = generateClients.iterator();
            while(itr.hasNext()) {
                Client client = (Client)itr.next();
                if(client.getArrivalTime() == currentTime){
                    scheduler.dispatchClient(client);//dispatch the arrived client
                    //notifyQueueServers(); // notify the queues that a new client is comming

                //  - delete client from list
                    itr.remove();
                }

            }
            //update UI frame
            updateUI(currentTime, this.outputFile);
            currentTime++;
            //wait an interval of 1 second;
            try{
            Thread.sleep(10);} //well 10 mills
            catch(InterruptedException e){
                System.out.println("SimulationManager thread interrupted");
                System.exit(5);
            }

        }
        //compute the average waiting time
        float avgWaitingTime = getAvgWaiting();
        lastUpdateUI(avgWaitingTime);//update the UI
        scheduler.finishThreads();// stop the queueServer threads
    }

    /**
     * The main method
     * @param args - the arguments must contain the input file followed by the output file
     * */
    public static void main(String[] args){
        SimulationManager gen = new SimulationManager(args);
        Thread t = new Thread(gen);
        t.start();


    }




}
