package com.project2.server;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @class QueueServer - Implements the runnable interface. It's instance is used
 * for creating a Thread object. It implements the capabilities of a queue.*/
public class QueueServer implements Runnable{
    //The synchronized dataStructure used for holding the clients(tasks)
    private BlockingQueue<Client> clients;
    //The total waiting period of all clients
    private AtomicInteger waitingPeriod;//read and write automatically
    //the average waiting period per client
    private int waitingPeriodPerClient;
    //the total number of clients that exited from queue
    private int totalServedClients;
    //the average waiting period for this queue given all the clients on it
    //for a new client that might want to know the expected waiting time;
    private float avgWaitingPeriod;
    //a flag that controls the flow of run() method of the queue com.com.project2.server
    //becames 0 when the thread terminates
    private AtomicInteger runFlag;


    public QueueServer(int queueCapacity){
        clients = new ArrayBlockingQueue(queueCapacity);
        waitingPeriod = new AtomicInteger(0);
        avgWaitingPeriod = 0;
        waitingPeriodPerClient = 1;
        totalServedClients = 0;
        runFlag = new AtomicInteger(1);

    }

    /*
    * addClient method - This method is used for adding a new client to the queue*/
    public void addClient(Client newClient) {
        try {
            clients.put(newClient);
        }catch(Exception e){
            System.out.println("Interrupted while putting client");
        }
        //incremet the waiting time;
        incrementAvgWaiting();
    }

    //Overwriting the run method of the Runnable interface
    public void run(){

        Client servedClinet; // the client that is currently served
        int waiting; // the waiting period for the last processed client

        while(runFlag.get() == 1){ // we use the runFlag variable to see when to terminate the thread

            if(clients.size() != 0) { // if the queue is not closed
                //we get the first client to process
                servedClinet = clients.peek();


                while (servedClinet.getProcessingTime() != 0) {
                    try {
                        Thread.sleep(10);
                        incrementFinishTimeForClients(); // increment the finishing time for all clients
                    } catch (InterruptedException e) {
                        System.out.println("Queue thread interrupted");
                    }
                    //set the processing time for the served client to be decremented
                    servedClinet.setProcessingTime(servedClinet.getProcessingTime() - 1);


                }
                //the waiting time of the servedClient is calculated as the difference between the
                // finishing time and the arival time
                waiting = servedClinet.getFinishTime() - servedClinet.getArrivalTime() ;
                try {
                    clients.take(); //  the client is removed form queue
                }catch(Exception e){
                    System.out.println("Interrupted while removing client");
                }
                totalServedClients++; // increment the number of served clients
                //we update all the variables dependent on the waiting period
                updateWaitingPeriods(waiting);
            }
            //-deprecated-
//            synchronized (clients) {
//                if (clients.size() == 0) // if the queue is empty keep it closed
//                    try {
//                        clients.wait(); // i.e call the wait() method on it
//                    } catch (InterruptedException e) {
//                        Thread.currentThread().interrupt();
//                        System.err.print("Thread interrupted");
//                        break;
//                    }
//            }

        }
    }

    //getter for clients queue
    public BlockingQueue<Client> getClients(){
        return this.clients;
    }
    //getter for size of the queue
    public int getSize(){
        return clients.size();
    }
    //getter for waiting period
    public int getWaitingTime(){
        return waitingPeriod.get();
    }

    //incrementAvgWaiting method - This method increments the average waiting time
    // for a new client that comes
    public void incrementAvgWaiting(){
        avgWaitingPeriod = waitingPeriodPerClient * clients.size();
    }
    //updateWaitingPeriods method - This method is used to update all the
    //variables that depend on the the waiting period;
    private void updateWaitingPeriods(int newWaiting){
        waitingPeriod.set(waitingPeriod.addAndGet(newWaiting)); //waiting period itself
        int waitPer = waitingPeriod.get();
        waitingPeriodPerClient = waitPer / totalServedClients; //an average waiting period per client
        // the average waiting period given all the clients in the queue
        avgWaitingPeriod = waitingPeriodPerClient * clients.size();
    }

    //incrementFinishingTimeForClients method - This method is used for incremneting
    //the finishing time for all the clients
    private void incrementFinishTimeForClients(){
        for(Client client : clients){
            client.incrementFinishTime();
        }
    }

    //stop method - Its main purpose is to stop the current thread by setting the runFlag to 0
    public void stop(){
        runFlag.set(0);
    }

    //getter for avgWaitingPeriod;
    public float getTime(){
        return this.avgWaitingPeriod;
    }

    /**getStringQueue mehtod - this method is used for computing the string representation
     * of all the clients present in the queue at a given time
     * @return string representation of all the clients present in the queue at a given time
     */

    public String getStringQueue(){
        if(clients.size() == 0)
            return ("closed"); //write closed if the queue is empty
        else {
            String auxString = "";
            for (Client client : clients) {
                auxString += client.getStringClient();
            }
            return auxString;
        }
    }
}
