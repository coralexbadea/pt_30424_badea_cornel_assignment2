package com.project2.server;

/**
 * @class Client - This class represents the task that will be given
 * to the queue thread to process. It represents abstraction of a client that
 * is characterized by ID, arrivalTime, processingTime, finishingTime.
 */
public class Client {
    private int ID;
    private int arrivalTime;
    private int processingTime;
    private int finishTime;


    //a simple constructor for Client
    public Client(int id, int arrivalTime, int processingTime) {
        this.ID = id;
        this.arrivalTime = arrivalTime;
        this.processingTime = processingTime;
        this.finishTime = arrivalTime;

    }
    /**
     * functional setters and getters*/
    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getID() {
        return ID;
    }

    public int getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(int processingTime) {
        this.processingTime = processingTime;
    }

    public int getFinishTime() {
        return finishTime;
    }



    /**
     * incrementFinishTime method - This method increments the finishTime of the client
     * with 1*/
    public void incrementFinishTime(){
        this.finishTime = this.finishTime + 1;
    }
    /**
     * getStringClient method - This method returns the characteristics of the client
     * in the form of a stirng
     * @return String*/
    public String getStringClient(){

        return ("("+this.getID()+","+this.getArrivalTime()+
                ","+this.getProcessingTime() +");");
    }
}
